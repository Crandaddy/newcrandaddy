import discord

import configparser
import os
import time
import asyncio
import discord
import central
from handlers.commands import CommandHandler 

dir_path = os.path.dirname(os.path.realpath(__file__))

config = configparser.ConfigParser()
config.read(f"{dir_path}/config.txt")

configVersion = configparser.ConfigParser()
configVersion.read(f"{dir_path}/config.example.txt")



class Crandaddy(discord.Client):

    def __init__(self,*args,  loop=None, **kwargs):
        super().__init__(*args, loop=loop, **kwargs)

        self.bg_task = self.loop.create_task(self.run_timed_loop())
        self.command_handler = CommandHandler()
        
    async def on_ready(self):
        await self.wait_until_ready()
        print('Logged on as {0}!'.format(self.user))


    async def unmute(self,res):
        for entry in res:
            try:
                guild = self.get_guild(entry['guild'])
                member = guild.get_member(entry['user'])
                if(guild == None):
                    raise KeyError("guild not found")
                elif(member == None):
                    raise KeyError("member not found")
            except:
                raise
            print(guild)                
            role = discord.utils.find(lambda m: m.name == 'Muted', guild.roles)
            if(role is None):                               
                return
            test = await member.remove_roles(role,reason="Unmuted")

    async def run_timed_loop(self):
        await self.wait_until_ready()
        
        while not self.is_closed():
            print("running loop")
        #current context
            ctx = {
                "self": bot,
                "author": "self",
                "identifier": "self",
                "channel": None,
                "message": None,
                "raw": None,
                "guild": None,
                "language": None
            }

            command = "monitorbans"
            remainder = None
        
            res = await self.command_handler.process_command(ctx, command,None)
            if(len(res['message']) > 0):
                await self.unmute(res['message'])
            
            #unmuted            
            print(res)
        
            await asyncio.sleep(60)

    async def on_guild_join(self,guild):
      
        #current context
        ctx = {
            "self": bot,
            "author": "self",
            "identifier": "self",
            "channel": None,
            "message": None,
            "raw": None,
            "guild": guild,
            "language": None
        }

        command = "join"
        remainder = None
        res = await self.command_handler.process_command(ctx, command,None)
        
        return None


    async def on_guild_remove(self,guild):
      
        #current context
        ctx = {
            "self": bot,
            "author": "self",
            "identifier": "self",
            "channel": None,
            "message": " ",
            "raw": None,
            "guild": guild,
            "language": None
        }

        command = "leave"
        res = await self.command_handler.process_command(ctx, command,None)
        
        return None
    
    async def on_message(self, raw):
        await self.wait_until_ready()

        #get messa state
        #current context
        #don't do it

        
        #query ban state       
        #current context
        ctx = {
            "self": bot,
            "author": raw.author,
            "identifier": f"{raw.author.name}#{raw.author.discriminator}",
            "channel": raw.channel,
            "message": raw.content,
            "raw": raw,
            "guild": raw.guild,
            "language": None
        }
        
        
        if config["Discord"]["devMode"] == "True":
            print('Message from channel {0[channel]}'.format(ctx))
            print('Message from {0[identifier]}: {0[message]}'.format(ctx))

        # parse command
        if ctx["message"].startswith(config["Discord"]["commandPrefix"]):
        
            command = ctx["message"][1:].split(" ")[0]
            remainder = " ".join(ctx["message"].split(" ")[1:])
        
            res = await self.command_handler.process_command(ctx, command, remainder)

            #send message !
            if res is not None:
                if res["message"] is not None:
                    await ctx["channel"].send(embed=res["message"])
                else:
                    await ctx["channel"].send("Done.")                    
                pass


        
        
bot= Crandaddy()
bot.run(config["Discord"]["token"])
