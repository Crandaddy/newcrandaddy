import configparser
import os
import time

import discord
#from handlers.commands import CommandHandler 

dir_path = os.path.dirname(os.path.realpath(__file__))

config = configparser.ConfigParser()
config.read(f"{dir_path}/config.txt")

configVersion = configparser.ConfigParser()
configVersion.read(f"{dir_path}/config.example.txt")
