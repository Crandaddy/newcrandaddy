import os
import json
import sqlite3
from sqlite3 import Error
import datetime
dir_path = os.path.dirname(os.path.realpath(__file__))    
def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)

def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)
 
    return None

def unmute(conn,server_idx,user_idx):
    cur = conn.cursor()
    entry = (server_idx,user_idx)
    sql = '''DELETE from mutes WHERE server_id=? AND user_id=?'''
    cur.execute(sql, entry)
    return cur.rowcount

def create_server_entry(conn, idx,names=""):
    cur = conn.cursor()
    entry = (idx,names)
    sql = ''' INSERT OR REPLACE INTO servers(server_id,server_name)
    VALUES(?,?)  '''
    cur = conn.cursor()
    cur.execute(sql, entry)
    return cur.lastrowid

def create_user_entry(conn, idx,names=""):
    cur = conn.cursor()
    entry = (idx,names)
    sql = ''' INSERT OR REPLACE INTO users(user_id,username)
    VALUES(?,?)  '''
    cur = conn.cursor()
    cur.execute(sql, entry)
    return cur.lastrowid

def create_ban_entry(conn,server_idx,user_idx,minutes=5,reason=""):
    cur = conn.cursor()

    date = str(datetime.datetime.now().date())
    time = f"{datetime.datetime.now().hour:02d}" + ':' + f"{datetime.datetime.now().minute:02d}" 
    entry = (server_idx,user_idx,date,time,minutes,reason)
    sql = ''' INSERT OR REPLACE INTO bans(server_id,user_id,date,time,minutes,reason)
    VALUES(?,?,?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, entry)
    return cur.lastrowid

def unban(conn,server_idx,user_idx):
    cur = conn.cursor()
    entry = (server_idx,user_idx)
    sql = '''DELETE from bans WHERE server_id=? AND user_id=?'''
    cur.execute(sql, entry)
    return cur.rowcount
    

def create_mute_entry(conn,server_idx,user_idx,minutes=5,reason=""):
    cur = conn.cursor()

    date = str(datetime.datetime.now().date())
    time = f"{datetime.datetime.now().hour:02d}:{datetime.datetime.now().minute:02d}" 
    print(time)
    entry = (server_idx,user_idx,date,time,minutes,reason)
    sql = ''' INSERT OR REPLACE INTO mutes(server_id,user_id,date,time,minutes,reason)
    VALUES(?,?,?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, entry)
    return cur.lastrowid

def query_mutes(conn,user_id =-1 ,server_id = -1 ,option = 0):
    cur = conn.cursor()
    if(option  == 0):
        cur.execute("SELECT mutes.*,users.username FROM mutes LEFT JOIN users ON mutes.user_id=users.user_id WHERE mutes.server_id=? AND mutes.user_id=?", (server_id,user_id,))
    elif(option == 1):
        cur.execute("SELECT mutes.*,users.username FROM mutes LEFT JOIN users ON mutes.user_id=users.user_id WHERE server_id=?", (server_id,))
    else:
        return 
    rows = cur.fetchall()
    return rows

def query_ban(conn,user_id =-1 ,server_id = -1 ,option = 0):
    cur = conn.cursor()
    if(option  == 0):
        cur.execute("SELECT bans.*,users.username FROM bans LEFT JOIN users ON bans.user_id=users.user_id WHERE bans.server_id=? AND bans.user_id=?", (server_id,user_id,))
    elif(option == 1):
        cur.execute("SELECT bans.*,users.username FROM bans LEFT JOIN users ON bans.user_id=users.user_id WHERE server_id=?", (server_id,))
    else:
        return 
    rows = cur.fetchall()
    return rows

def query_all_mutes(conn):
    cur = conn.cursor()
    cur.execute("SELECT * FROM mutes")
    rows = cur.fetchall()
    return rows

def query_all_bans(conn):
    cur = conn.cursor()
    cur.execute("SELECT bans.*,users.username FROM bans LEFT JOIN users ON bans.user_id=users.user_id")
    rows = cur.fetchall()
    return rows

def create_tables(conn):
    sql_create_servers_table = """CREATE TABLE IF NOT EXISTS servers (
                                server_id integer PRIMARY KEY,
                                server_name text
                                );"""

    sql_create_users_table = """CREATE TABLE IF NOT EXISTS users (
                                user_id integer PRIMARY KEY,
                                username text
                                );"""

    sql_create_ban_table = """CREATE TABLE IF NOT EXISTS bans (
                                server_id integer NOT NULL,
                                user_id integer NOT NULL,
                                date text NOT NULL,
                                time text NOT NULL,
                                minutes integer NOT NULL,
                                reason text,
                                FOREIGN KEY (server_id) REFERENCES servers(server_id) ON DELETE CASCADE,
                                FOREIGN KEY (user_id)   REFERENCES servers(user_id) ON DELETE CASCADE,
                                CONSTRAINT UC_Person UNIQUE (server_id,user_id)
                                );"""

    sql_create_mutes_table = """CREATE TABLE IF NOT EXISTS mutes (
                                server_id integer NOT NULL,
                                user_id integer NOT NULL,
                                date text NOT NULL,
                                time text NOT NULL,
                                minutes integer NOT NULL,
                                reason text,
                                FOREIGN KEY (server_id) REFERENCES servers(server_id) ON DELETE CASCADE,
                                FOREIGN KEY (user_id)   REFERENCES servers(user_id) ON DELETE CASCADE,
                                CONSTRAINT UC_Person UNIQUE (server_id,user_id)
                                );"""

    sql_create_roles_table = """CREATE TABLE IF NOT EXISTS roles (
                                server_id integer NOT NULL,
                                user_id integer NOT NULL,
                                roles integer NOT NULL,
                                FOREIGN KEY (server_id) REFERENCES servers(server_id) ON DELETE CASCADE,
                                FOREIGN KEY (user_id)   REFERENCES servers(user_id) ON DELETE CASCADE
                                CONSTRAINT UC_Person UNIQUE (server_id,user_id)
                                );"""

    create_table(conn, sql_create_servers_table)
    create_table(conn, sql_create_users_table)
    create_table(conn, sql_create_ban_table)
    create_table(conn, sql_create_mutes_table)
    create_table(conn, sql_create_roles_table)




def main():
    database = f"{dir_path}/../../../databases/moddb.db"
    sql_create_servers_table = """CREATE TABLE IF NOT EXISTS servers (
                                server_id integer PRIMARY KEY,
                                server_name text
                                );"""

    sql_create_users_table = """CREATE TABLE IF NOT EXISTS users (
                                user_id integer PRIMARY KEY,
                                username text
                                );"""

    sql_create_ban_table = """CREATE TABLE IF NOT EXISTS bans (
                                server_id integer NOT NULL,
                                user_id integer NOT NULL,
                                date text NOT NULL,
                                time text NOT NULL,
                                minutes integer NOT NULL,
                                reason text,
                                FOREIGN KEY (server_id) REFERENCES servers(server_id) ON DELETE CASCADE,
                                FOREIGN KEY (user_id)   REFERENCES servers(user_id) ON DELETE CASCADE,
                                CONSTRAINT UC_Person UNIQUE (server_id,user_id)
                                );"""

    sql_create_mutes_table = """CREATE TABLE IF NOT EXISTS mutes (
                                server_id integer NOT NULL,
                                user_id integer NOT NULL,
                                date text NOT NULL,
                                time text NOT NULL,
                                minutes integer NOT NULL,
                                reason text,
                                FOREIGN KEY (server_id) REFERENCES servers(server_id) ON DELETE CASCADE,
                                FOREIGN KEY (user_id)   REFERENCES servers(user_id) ON DELETE CASCADE,
                                CONSTRAINT UC_Person UNIQUE (server_id,user_id)
                                );"""

    sql_create_roles_table = """CREATE TABLE IF NOT EXISTS roles (
                                server_id integer NOT NULL,
                                user_id integer NOT NULL,
                                roles integer NOT NULL,
                                FOREIGN KEY (server_id) REFERENCES servers(server_id) ON DELETE CASCADE,
                                FOREIGN KEY (user_id)   REFERENCES servers(user_id) ON DELETE CASCADE
                                CONSTRAINT UC_Person UNIQUE (server_id,user_id)
                                );"""
    
    # create a database connection
    print(database)
    try:
        conn = create_connection(database)
        print(conn)
    except:
        print("database")
        raise 
    with conn:
        """
        create_table(conn, sql_create_servers_table)
        create_table(conn, sql_create_users_table)
        create_table(conn, sql_create_ban_table)
        create_table(conn, sql_create_mutes_table)
        create_table(conn, sql_create_roles_table)
        
        create_user_entry(conn,0,"a")
        create_user_entry(conn,1,"b")
        create_user_entry(conn,2,"b")
        create_server_entry(conn,0,"a")
        create_ban_entry(conn,0,0,5,"wtf")
        create_ban_entry(conn,0,1,5,"wtf")
        create_ban_entry(conn,0,2,5,"wtf")
        create_mute_entry(conn,0,2,5,"wtf")
        """
        print( query_all_bans(conn))
        print( query_all_mutes(conn))

    
if (__name__ == "__main__"):       
    main()     
