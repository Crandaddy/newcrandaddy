import moddatabase
import os
import datetime
import discord
dir_path = os.path.dirname(os.path.realpath(__file__))

class Moderation:
    conn = None
    def __init__(self):
        database = f"{dir_path}/../../../databases/moddb.db"
        self.conn = moddatabase.create_connection(database)
        print(self.conn)
        with self.conn:
            moddatabase.create_tables(self.conn)

    async def banUser(self,guild,args):
        with self.conn:
            server_idx = guild.id

            e = discord.Embed()
            try:
                print(args)

                print(guild)
                member = guild.get_member_named(args[0])
                time = args[1]
                reason = args[2]                
                
                print(member)
                user_idx = member.id 
                if(member is not None):
                    nick = member.name
                else:
                    nick = ""
                #create user if it doesn't exist
                moddatabase.create_user_entry(self.conn,user_idx,nick)
                moddatabase.create_ban_entry(self.conn,server_idx,user_idx,
                                             time,reason)
                e.add_field(name="Result", value=f"banned {member}({nick}) for {time} minutes", inline=False)
            except IndexError:
                e.add_field(name="Command:", value="!ban <member> <time> <reason>", inline=False)
                e.add_field(name="Values", value="<member>: name (right click on member and copy name#identifer)\n <time>: Time in minutes \n <reason>:reason", inline=False)
            return e
        
    def unbanUser(self,server_idx,user_idx):
        e = discord.Embed()
        with self.conn:
            nobanned = moddatabase.unban(self.conn,server_idx,user_idx)
            if(nobanned == 0):
                e.add_field(name="Error", value="Member is not banned", inline=False)

    def queryBans(self,guild):
        e = discord.Embed()
        with self.conn:
            data = moddatabase.query_ban(self.conn,
                                         server_id=guild.id,
                                         option=1)
            if(len(data) > 0):
                for entry in data:
                    e.add_field(name="Member", value=f"name:{entry[6]}\nid: {str(entry[1])}\nBanned on:{entry[2]} {entry[3]}\nDuration:{entry[4]}\nReason:{entry[5]}",inline=False)
            else:
                e.add_field(name="Member", value="No one banned", inline=False)
            return e

    def queryMutes(self,guild):
        e = discord.Embed()
        with self.conn:
            data = moddatabase.query_mutes(self.conn,
                                         server_id=guild.id,
                                         option=1)
            if(len(data) > 0):
                for entry in data:
                    e.add_field(name="Member", value=f"name:{entry[6]}\nid: {str(entry[1])}\nMuted on:{entry[2]} {entry[3]}\nDuration:{entry[4]} mins \nReason:{entry[5]}",inline=False)
            else:
                e.add_field(name="Member", value="No one is muted on this server", inline=False)
            return e

    def queryBanByMessage(self,guild,member):
        e = discord.Embed()
        server_idx = guild.id
        user_idx = member.id
        data = moddatabase.query_ban(self.conn,
                                     user_id=user_idx,
                                     server_id=server_idx,option=0)
        if(len(data) > 0):
            return True
        else:
            return False


    def queryMuteByMessage(self,guild,member):
        e = discord.Embed()
        server_idx = guild.id
        user_idx = member.id
        
        with self.conn:
            data = moddatabase.query_mutes(self.conn,
                                         user_id=user_idx,
                                         server_id=server_idx)
        if(len(data) > 0):
            return True
        else:
            return False

    def queryMuteByUser(self,guild,args):
        e = discord.Embed()
        try:
            server_idx = guild.id
            member = guild.get_member_named(args[0])
            user_idx = member.id
        except KeyError:
            e.add_field(name="Command:", value="!querymute <memberid>", inline=False)
            return e
        
        with self.conn:
            data = moddatabase.query_mutes(self.conn,
                                         user_id=user_idx,
                                         server_id=server_idx,option=0)
 
            if(len(data) > 0):
                e.add_field(name="Name", value=f"{data[0][6]}", inline=False)
                e.add_field(name="ID", value=str(user_idx), inline=False)
                e.add_field(name="Muted on", value=f"{data[0][2]} {data[0][3]}", inline=False)
                e.add_field(name="Duration", value=f"{data[0][4]}", inline=False)
                e.add_field(name="Reason", value=f"{data[0][5]}", inline=False)
            else:
                e.add_field(name="Error", value="Member is not banned", inline=False)
            return e
        
        
    def queryBanByUser(self,guild,args):
        e = discord.Embed()
        try:
            server_idx = guild.id
            member = guild.get_member_named(args[0])
            user_idx = member.id
        except KeyError:
            e.add_field(name="Command:", value="!queryban <memberid>", inline=False)
            return e
        
        with self.conn:
            data = moddatabase.query_ban(self.conn,
                                         user_id=user_idx,
                                         server_id=server_idx,option=0)
 
            if(len(data) > 0):
                e.add_field(name="Name", value=f"{data[0][6]}", inline=False)
                e.add_field(name="ID", value=str(user_idx), inline=False)
                e.add_field(name="Banned on", value=f"{data[0][2]} {data[0][3]}", inline=False)
                e.add_field(name="Duration", value=f"{data[0][4]}", inline=False)
                e.add_field(name="Reason", value=f"{data[0][5]}", inline=False)
            else:
                e.add_field(name="Error", value="Member is not banned", inline=False)
            return e
        
    async def monitorBans(self):
        data = moddatabase.query_all_bans(self.conn)
        for row in data:                        
            date_time_obj = datetime.datetime.strptime(f'{row[2]} {row[3]}', '%Y-%m-%d %H:%M')
            time_since = (datetime.datetime.now() - date_time_obj)
            diff = int(time_since.total_seconds()/60)
            if(diff > row[4]):
                print(f"unbanning {row[1]} as time is up")
                with self.conn:
                    moddatabase.unban(self.conn,row[0],row[1])
        pass

    async def monitorMutes(self):
        data = moddatabase.query_all_mutes(self.conn)

        res = []
        count = 0
        for row in data:                        
            date_time_obj = datetime.datetime.strptime(f'{row[2]} {row[3]}', '%Y-%m-%d %H:%M')
            time_since = (datetime.datetime.now() - date_time_obj)
            diff = int(time_since.total_seconds()/60)
            if(diff > row[4]):
                print(f"unmuting {row[1]} as time is up")
                with self.conn:
                    print(f"arguments: {row[0]} {row[1]}")
                    if(moddatabase.unmute(self.conn,row[0],row[1]) >0):
                        data ={'guild':row[0],'user':row[1]}
                        res.append(data)
                        count +=1

                    
        print(f"unmuted {count} user")
        return res

    async def muteUser(self,guild,args):
        e = discord.Embed()
        with self.conn:

            
            role = discord.utils.find(lambda m: m.name == 'Muted', guild.roles)
            
            if(role is None):                                
                e.add_field(name="Result", value=f"A 'Muted' role doesn't exist!", inline=False)
                return e
            
            member = guild.get_member_named(args[0])
            if member is not None:
                time = args[1]
                reason = args[2]      
                await member.add_roles(role,reason="Muted")
                
                
                user_idx = member.id
                nick = member.name
                server_idx = guild.id
            
                #create user if it doesn't exist
                moddatabase.create_user_entry(self.conn,user_idx,nick)
                moddatabase.create_mute_entry(self.conn,server_idx,user_idx,
                                              time,reason)


                #assign muted role 

                
                e.add_field(name="Result", value=f"muted {member}({nick}) for {time} minutes", inline=False)
            else:
                e.add_field(name="Command:", value="!muted <member> <time> <reason>", inline=False)
                e.add_field(name="Values", value="<member>: name (right click on member and copy name#identifer)\n <time>: Time in minutes \n <reason>:reason", inline=False)
        return e
    
if(__name__=="__main__"): 
    class User:
        def __init__(self,idx,name):
            self.id = idx
            self.name = name
    mod = Moderation()
    user = User(2,"James")
    user2 = User(3,"Dan")
    #mod.banUser(0,user)
    #mod.banUser(1,user2)
    print(mod.monitorBans())
    print(mod.queryBans(0))
    print(mod.queryBans(1))
  
