import discord
import json
import os
import toSQL
dir_path = os.path.dirname(os.path.realpath(__file__))

try:
    with open(f'{dir_path}/memes.json') as json_data:
        memes = json.load(json_data)
except:
    with open(f'{dir_path}/memes.json.example') as json_data:
        memes = json.load(json_data)
    
class Memes:
    def __init__(self):
        database = f"{dir_path}/../../../databases/memes.db"
        self.conn = toSQL.create_connection(database)
        toSQL.create_tables(self.conn)
        pass

    def add_server(self,guild):
        e = discord.Embed()
        idx = toSQL.create_server(self.conn,guild)
        e.add_field(name ="Avaliable Memes:",value=idx, inline=False)
        return e

    def del_server(self,guild):
        e = discord.Embed()
        try:
            status = toSQL.delete_server(self.conn,guild)
            e.add_field(name ="Status",value="Data Deleted", inline=False)
        except:
            e.add_field(name ="Status",value="Something is buggered when leaving the server LULW", inline=False)         
        return e

        
    def post_memes(self,guild,command):
        e = discord.Embed()

        if(len(command) == 0):
            return self.listMeme(guild)
        print(guild)
        try:
            with self.conn:
                url = toSQL.select_meme_by_name(self.conn,guild,command)
        except KeyError:
            e.set_footer(text="Meme does not exist!")
            return e
 
        e.set_image(url=url)        
        return e
            
    def addMeme(self,guild,command):
        e = discord.Embed()
        try:
            name = "".join(command.split(" ")[0])
            url = "".join(command.split(" ")[1])
        except:
            e.set_footer(text="Not enough arugment : !addmeme <name> <url>")
            return e
        with self.conn:
            toSQL.create_meme(self.conn,guild,name,url)

        e.set_footer(text="Meme added")
        return e

    def delMeme(self,guild,remainder):
        e = discord.Embed()
        try:
            name = "".join(remainder.split(" ")[0])
        except:
            e.set_footer(text="Not enough arugment : !delmeme <name>")
            return e
        

        print("deleting meme")
        try:
            with self.conn:
                toSQL.delete_meme(self.conn,guild,name)
            e.set_footer(text="Meme deleted")
            return e
        except:
            e.set_footer(text="Meme does not exist!")
            return e

    def listMeme(self,guild):
        e = discord.Embed()
        e.add_field(name="Command:", value="!meme <name>", inline=False)
        string = ""
        with self.conn:
            list_memes=toSQL.list_meme(self.conn,guild)
        print("list of memes: " + str(list_memes))
        if(len(list_memes) > 0):
            for n in list_memes:
                print(n)
                string +="{0}\n".format(n[0])
        else:
            string = "No Memes Avaliable"

        e.add_field(name ="Avaliable Memes:",value=string, inline=False)
            
        return e
