import os
import json
import sqlite3
from sqlite3 import Error
dir_path = os.path.dirname(os.path.realpath(__file__))


try:
    with open(f'{dir_path}/memes.json') as json_data:
        memes = json.load(json_data)
except:
    with open(f'{dir_path}/memes.json.example') as json_data:
        memes = json.load(json_data)


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)
 
    return None

def lookup_id(conn,server_id):
    try:
        return select_server_by_server_id(conn,server_id)
    except KeyError:
        return create_server(conn,server_id)
        


    

    
def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)

def create_tables(conn):
    sql_create_servers_table = """CREATE TABLE IF NOT EXISTS servers (
                                id integer PRIMARY KEY,    
                                server_id integer
                                );"""
    
    sql_create_memes_table = """CREATE TABLE IF NOT EXISTS memes (
                                server_id integer NOT NULL,
                                name text,
                                url text,
                                FOREIGN KEY (server_id) REFERENCES servers(server_id) ON DELETE CASCADE
                                );"""
    create_table(conn, sql_create_servers_table)
    create_table(conn, sql_create_memes_table)
    
def update_meme_by_idx(conn,idx,name,url):
    sql = ''' UPDATE memes
              SET url = ? 
              WHERE server_id = ? AND name = ?'''
    cur = conn.cursor()
    cur.execute(sql, (url,idx,name,))
    return True

    
        
def create_meme(conn, server_id,name,url ):
    cur = conn.cursor()

    #ask for server
    idx = lookup_id(conn,server_id)
        
    try:
        select_meme_by_idx(conn,idx,name)
        return update_meme_by_idx(conn,idx,name,url)
        
    except KeyError:
        #meme not found
        meme = (idx,name,url)
        sql = ''' INSERT INTO memes(server_id,name,url)
        VALUES(?,?,?) '''
        cur = conn.cursor()
        cur.execute(sql, meme)
    return cur.lastrowid


def delete_meme(conn, server_id,name):
    cur = conn.cursor()
    
    idx = lookup_id(conn,server_id)
    try:
        select_meme_by_idx(conn,idx,name)
    except KeyError:
        print("meme not found")
        raise
        
    sql = '''DELETE FROM memes WHERE name=?'''
    cur = conn.cursor()
    cur.execute(sql, (name,))
    print(cur.fetchall())
    return True

def create_server(conn, server):
    #check number of rows
    sql = '''SELECT COALESCE(MAX(id)+1, 0) FROM servers'''
    cur = conn.cursor()
    cur.execute(sql)
    rows = cur.fetchall()
    
    server = (rows[0][0]+1,server)
    sql = ''' INSERT INTO servers(id,server_id)
              VALUES(?,?) '''
    cur = conn.cursor()
    cur.execute(sql, server)
    return rows[0][0]+1

def delete_server(conn, server):   
    server = (rows[0][0]+1,server)
    try:
        sql = '''DELETE FROM server WHERE server=?'''
        cur = conn.cursor()
        cur.execute(sql, (server,))
        return True
    except:
        raise 




def select_meme_by_idx(conn,idx,name):
    cur = conn.cursor()
    print("id :{0}".format(idx))
  
    cur.execute("SELECT url FROM memes WHERE server_id=? AND name=?", (idx,name,))

    rows = cur.fetchall()
    if(len(rows) > 0):
        return rows[0][0]
    else:
        raise KeyError("Meme not found")

def select_meme_by_name(conn, server_id, name):
    cur = conn.cursor()
    idx = lookup_id(conn,server_id)
        
    return select_meme_by_idx(conn,idx,name)

def select_server_by_server_id(conn, server_id):
    """
    Query tasks by priority
    :param conn: the Connection object
    :param priority:
    :return:
    """
    cur = conn.cursor()
    
    cur.execute("SELECT * FROM servers WHERE server_id=?", (server_id,))
    
    rows = cur.fetchall()
    
    if(len(rows) > 0):
        return rows[0][0]
    else:
        raise KeyError
    

def list_meme(conn,server_id):
    cur = conn.cursor()
    idx = lookup_id(conn,server_id)
    
    cur.execute("SELECT name FROM memes WHERE server_id=?", (idx,))

    rows = cur.fetchall()
    return rows

