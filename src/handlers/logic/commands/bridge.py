
import os
import sys
import discord

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(dir_path + "/../memes")
sys.path.append(dir_path + "/../moderation")
sys.path.append(dir_path + "/../..")
import central

print(sys.path)
from memeParser import Memes
from moderate import Moderation
memes = Memes()
mod = Moderation()

async def run_command(ctx, command, remainder):

    embed = discord.Embed()
    lang = ctx["language"]
    user = ctx["author"]
    guild = ctx["guild"]
    channel = ctx["channel"]
    args = remainder.split(" ")
    
    if central.config["Discord"]["devMode"] == "True":
        print("Normal command")

    
    if(mod.queryBanByMessage(guild,user)):
        embed.add_field(name ="Error:",value="You are banned from Crandaddy!", inline=False)
        return {
            "level": "error",
            "message": embed
        }


        
    if(command == "meme"):
        embed = memes.post_memes(guild.id,remainder)    
        return {
            "level": "info",
            "message": embed
        }
    
    return None

async def run_self_command(ctx, command, remainder):
    embed = discord.Embed()
    embed.add_field(name ="Owner:",value="Internal Command Placeholder", inline=False)
    lang = ctx["language"]
    user = ctx["author"]
    guild = ctx["guild"]
    channel = ctx["channel"]
    print(remainder)
    args = remainder
    

    if(command == "join"):
        server = memes.add_server(guild.id)
        embed.add_field(name="", value="Rend your heart, and not your garments, and turn unto the Lord your God: for he is gracious and merciful, slow to anger, and of great kindness, and repenteth him of the evil.", inline=False)
    elif(command == "leave"):
        server = memes.del_server(guild.id)
    elif(command == "monitorbans"):
        await mod.monitorBans()
        embed =  await mod.monitorMutes()
    elif(command == "getmute"):
        embed =  mod.queryMuteByMessage(guild,remainder)   
    else:
        pass
    
    return {
        "level": "info",
        "message": embed
    }



    
async def run_mod_command(ctx, command, remainder):
    embed = discord.Embed()
    lang = ctx["language"]
    user = ctx["author"]
    guild = ctx["guild"]
    channel = ctx["channel"]
    args = remainder.split(" ")
    
    if central.config["Discord"]["devMode"] == "True":
        print("Owner command")

    if(command == "addmeme"):
        embed =  memes.addMeme(guild.id,remainder)
        return {
            "level": "info",
            "message": embed
        }   
    elif(command == "delmeme"):
        embed = memes.delMeme(guild.id,remainder)
        return {
            "level": "info",
            "message": embed
        }
    elif(command == "ban"):
        e = await mod.banUser(guild,args)
        return {
            "level": "info",
            "message": e
        }
    elif(command == "mute"):
        e = await mod.muteUser(guild,args)
        return {
            "level": "info",
            "message": e
        }
    elif(command == "unmute"):
        e = await mod.banUser(guild,args)
        return {
            "level": "info",
            "message": e
        }
    elif(command == "queryban"):
        print(args)
        if(len(args) == 1):
            e = mod.queryBans(guild)
        else:
            e = mod.queryBanByUser(guild,args)
        return {
            "level": "info",
            "message": e
        }
    elif(command == "querymute"):
        print(args)
        if(len(args) == 1):
            e = mod.queryMutes(guild)
        else:
            e = mod.queryMutesByUser(guild,args)
        return {
            "level": "info",
            "message": e
        }
    
    return None



async def run_owner_command(ctx, command, remainder):
    embed = discord.Embed()
    lang = ctx["language"]
    user = ctx["author"]
    guild = ctx["guild"]
    channel = ctx["channel"]
    args = remainder.split(" ")

    embed.add_field(name ="Owner:",value="Owner Command Placeholder", inline=False)



    
    return {
        "level": "info",
        "message": embed
    }
