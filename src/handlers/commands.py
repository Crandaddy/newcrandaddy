import central
import os
import sys

from handlers.logic.commands import bridge
from discord import Permissions

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(dir_path + "/..")
import central

class CommandHandler:
    mod_commands = ["userid", "ban", "unban", "mute", "unmute",
                    "userstat","addmeme","delmeme","queryban","querymute"]
    
    owner_commands = ["owner"]
    self_commands = ["join","leave","monitorbans","getmute"]
    @classmethod
    def is_owner_command(self,command):
        if command in self.owner_commands:
            return True
        else:
            return False

    @classmethod
    def is_mod(self,ctx):
        return ctx["author"].guild_permissions.manage_guild 

    @classmethod
    def is_owner(self,ctx):
        return str(ctx["author"].id) == central.config["Discord"]["owner"]

    @classmethod
    def is_self(self,ctx):
        return str(ctx["author"]) == "self"

    @classmethod
    def is_self_command(self,command):
        return (command in self.self_commands)
    
    @classmethod
    def is_mod_command(self,command):
        return (command in self.mod_commands)
    
    @classmethod
    async def process_command(self, ctx, command, remainder=None):
        if not (self.is_self(ctx) or self.is_owner_command(command) or self.is_mod_command(command)):
            return await bridge.run_command(ctx, command, remainder)
        else:
            if(self.is_self(ctx) and self.is_self_command(command)):
                return await bridge.run_self_command(ctx, command, remainder)           
            elif ( self.is_mod(ctx) and self.is_mod_command(command)):
                return await bridge.run_mod_command(ctx, command, remainder)
            elif(self.is_owner(ctx)  and self.is_owner_command(command)):
                return await bridge.run_owner_command(ctx, command, remainder)

                
